![RolandUINO](https://framagit.org/fablab-renens/repos/raw/master/images/RolandUINO/rolanduino.png "Image RolandUINO")
# [RolandUINO](https://framagit.org/fablab-renens/RolandUINO/wikis/home)
**_Remise en service et hacking d'une CNC 'Roland Modela MDX-20'_**

La documentation du projet est disponible sur le [wiki](https://framagit.org/fablab-renens/RolandUINO/wikis/home).
