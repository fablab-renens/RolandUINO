Le fichier '__GRBL_Configuration.txt__' contient la liste des valeurs associées aux paramètres de GRBL pour notre Roland Modela MDX-20.  
Ces valeurs sont sauvegardées en EEPROM sur l'Arduino et on y accède avec la commande `$$`.  
Plus de détails dans la section [Arduino & GRBL](https://framagit.org/fablab-renens/RolandUINO/wikis/Arduino-&-GRBL) du wiki... et différentes informations sur ce paramétrage final [ici](https://framagit.org/fablab-renens/RolandUINO/wikis/Configuration-finale-et-essais).  
